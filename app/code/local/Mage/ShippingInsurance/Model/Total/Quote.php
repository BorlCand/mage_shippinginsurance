<?php

class Mage_ShippingInsurance_Model_Total_Quote extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'shipping_insurance';

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        /** @var $helper Mage_ShippingInsurance_Helper_Data */
        $helper = Mage::helper('shippinginsurance');

        if ($address->getShippingMethodInsurance()) {
            $costInsurance = $address->getShippingInsurance();
            $address->addTotal(
                array(
                    'code'  => $this->getCode(),
                    'title' => $helper->getTranslatedLabel(),
                    'value' => $costInsurance
                )
            );
        }

        return $this;
    }

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        /** @var $helper Mage_ShippingInsurance_Helper_Data */
        $helper = Mage::helper('shippinginsurance');

        if (!$helper->isFeatureEnabled() || !count($this->_getAddressItems($address))) {
            return $this;
        }

        if ($address->getShippingMethodInsurance() && $address->getShippingInsurance()) {
            $address->setGrandTotal($address->getGrandTotal() + $address->getShippingInsurance());
            $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getShippingInsurance());

            $quote = $address->getQuote();
            $quote->setGrandTotal($quote->getGrandTotal() + $quote->getShippingInsurance());
            $quote->setBaseGrandTotal($quote->getBaseGrandTotal() + $quote->getShippingInsurance());
        }

        return $this;
    }
}
