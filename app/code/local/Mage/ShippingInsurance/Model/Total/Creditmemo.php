<?php

class Mage_ShippingInsurance_Model_Total_Creditmemo extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        /** @var $helper Mage_ShippingInsurance_Helper_Data $helper */
        $helper = Mage::helper('shippinginsurance');

        $order = $creditmemo->getOrder();
        $costInsurance = $order->getShippingInsurance();

        if ($helper->isFeatureEnabled() && $order->getShippingMethodInsurance()) {
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $costInsurance);
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $costInsurance);
        }

        return $this;
    }
}
