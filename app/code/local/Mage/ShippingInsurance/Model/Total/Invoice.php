<?php

class Mage_ShippingInsurance_Model_Total_Invoice extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        /** @var $helper Mage_ShippingInsurance_Helper_Data $helper */
        $helper = Mage::helper('shippinginsurance');

        $order = $invoice->getOrder();
        $costInsurance = $order->getShippingInsurance();

        if ($helper->isFeatureEnabled() && $order->getShippingMethodInsurance()) {
            $invoice->setGrandTotal($invoice->getGrandTotal() + $costInsurance);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $costInsurance);
        }

        return $this;
    }
}