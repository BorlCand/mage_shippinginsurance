<?php

class Mage_ShippingInsurance_Model_Observer
{
    public function checkoutControllerOnepageSaveShippingMethod(Varien_Event_Observer $observer)
    {
        /** @var $helper Mage_ShippingInsurance_Helper_Data $helper */
        $helper = Mage::helper('shippinginsurance');

        $moduleEnabled = $observer->getRequest()->getParam('shippinginsurance_enabled', false);

        $quote = $observer->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        $shippingMethod = $shippingAddress->getShippingMethod();
        $shippingRates = $shippingAddress->collectShippingRates()->getGroupedAllShippingRates();

        $shippingAddress->setShippingMethodInsurance(null);
        $shippingAddress->setShippingInsurance(0);

        $quote->setShippingMethodInsurance(null);
        $quote->setShippingInsurance(0);

        // return without calculation if insurance is disabled
        if (!$helper->isFeatureEnabled() || !$moduleEnabled) {
            return $this;
        }

        $carrierCode = null;

        foreach ($shippingRates as $rate) {
            foreach ($rate as $carrier) {
                if ($shippingMethod === $carrier->getCode()) {
                    $carrierCode = $carrier->getCarrier();
                    break;
                }
            }
        }

        // carrier has not been found or is not allowed by insurance settings
        if (!$carrierCode || !$helper->isCarrierCodeAllowed($carrierCode)) {
            return $this;
        }

        $cost = $helper->calculateInsuranceCost($carrierCode, $quote->getSubtotal());

        $shippingAddress->setShippingMethodInsurance($carrierCode);
        $shippingAddress->setShippingInsurance($cost);

        $quote->setShippingMethodInsurance($carrierCode);
        $quote->setShippingInsurance($cost);

        return $this;
    }
}
