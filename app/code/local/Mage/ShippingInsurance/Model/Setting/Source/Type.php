<?php

class Mage_ShippingInsurance_Model_Setting_Source_Type
{
    const TYPE_FIXED  = 1;
    const TYPE_FIXED_LABEL = 'Flat';

    const TYPE_PERCENT  = 2;
    const TYPE_PERCENT_LABEL = 'Percent';

    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::TYPE_FIXED,
                'label' => self::TYPE_FIXED_LABEL
            ),
            array(
                'value' => self::TYPE_PERCENT,
                'label' => self::TYPE_PERCENT_LABEL
            ),
        );
    }
}
