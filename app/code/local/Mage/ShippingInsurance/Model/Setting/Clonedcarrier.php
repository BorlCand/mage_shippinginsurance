<?php

class Mage_ShippingInsurance_Model_Setting_Clonedcarrier extends Mage_Core_Model_Config_Data
{
    public function getPrefixes()
    {
        $prefixes = array();
        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();

        /** @var  $carrier Mage_Shipping_Model_Carrier_Abstract */
        foreach ($carriers as $carrier) {
            $prefixes[] = array(
                'label' => $carrier->getConfigData('title'),
                'field' => sprintf('field_%s_', $carrier->getCarrierCode()),
            );
        }
        
        return $prefixes;
    }
}
