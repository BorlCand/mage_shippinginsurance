<?php

class Mage_ShippingInsurance_Helper_Data extends Mage_Core_Helper_Data
{
    public function getTranslatedLabel()
    {
        return $this->__('Shipping Insurance');
    }

    public function isFeatureEnabled()
    {
        return (bool) Mage::getStoreConfig('shippinginsurance/settings/enabled');
    }

    public function isCarrierCodeAllowed($carrierCode)
    {
        if (!is_string($carrierCode) || empty($carrierCode)) {
            Mage::log('Shipping Method is invalid');

            return false;
        }

        return (bool) Mage::getStoreConfig(sprintf(
            'shippinginsurance/rates/field_%s_enabled',
            $carrierCode
        ));
    }

    public function calculateInsuranceCost($carrierCode, $subTotal)
    {
        if (!is_string($carrierCode) || empty($carrierCode)) {
            Mage::log('Invalid Carrier Code');

            return 0;
        }

        $type = Mage::getStoreConfig('shippinginsurance/rates/field_' . $carrierCode . '_type');
        $fee = Mage::getStoreConfig('shippinginsurance/rates/field_' . $carrierCode . '_fee');

        switch ($type) {
            case Mage_ShippingInsurance_Model_Setting_Source_Type::TYPE_FIXED:
                return round($fee, 2, PHP_ROUND_HALF_UP);
                break;

            case Mage_ShippingInsurance_Model_Setting_Source_Type::TYPE_PERCENT:
                return round($subTotal * ($fee / 100), 2, PHP_ROUND_HALF_UP);
                break;

            default:
                Mage::log('Carrier code is not supported or calculation not implemented.');
                return 0;
        }
    }

    public function rewriteTotals($class)
    {
        $order = $class->getOrder();

        if ($order->getShippingMethodInsurance()) {
            $costInsurance = $order->getShippingInsurance();
            $class->addTotalBefore(
                new Varien_Object(
                    array(
                        'code' => $class->getCode(),
                        'value' => $costInsurance,
                        'base_value' => $costInsurance,
                        'label' => $this->getTranslatedLabel()
                    ),
                    'grand_total'
                )
            );
        }
    }
}
